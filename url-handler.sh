#!/bin/bash

link=$@
progs='Firefox\n
MPV\n
Chromium\n
Firefox (new window)\n
Firefox (private window)\n
Clipboard'

browser=$(echo -e $progs | dmenu)

if [[ $browser == "Firefox" ]]; then
	firefox $link
elif [[ "$browser" =~ MPV ]]; then
	mpv $link
elif [[ "$browser" =~ Chromium ]]; then
	chromium $link
elif [[ "$browser" =~ "Firefox (new window)" ]]; then
	firefox --new-window $link
elif [[ "$browser" =~ "Firefox (private window)" ]]; then
	firefox --private-window $link
elif [[ "$browser" =~ Clipboard ]]; then
	echo -n $link | xclip -selection "clipboard"
fi
