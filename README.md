# url-handler

Dmenu-based script to choose different handlers when selecting URLs in the Terminal.

I use it to select links in the terminal (urxvt) and want to have a choice which program
to use. The alternative would be to select and copy each link which is easy to get wrong.
The configuration in the `Xresources`-file ( `.Xresources` in my home) chooses some
modules for urxvt including the `matcher` which finds the URLs in the text.
The script in this repo is then chosen as the handler, which gets the url as an argument.
Also the right mouse button is chosen to activate the handler, which prevents accidental
clicking on links while trying to copy something.

An alternative would be to select this script as the default application for xdg-open.
